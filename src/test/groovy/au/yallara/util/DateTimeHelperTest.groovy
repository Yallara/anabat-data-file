/*
 * Copyright (c) 2014. Anne Jessel
 *
 * This file is part of AnabatDataFile.
 *
 * AnabatDataFile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AnabatDataFile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AnabatDataFile.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.util

import au.yallara.util.DateTimeHelper
import spock.lang.Specification

import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit

class DateTimeHelperTest extends Specification {

    private DateTimeHelper subject
    private static final ZoneId melbourneTZ = ZoneId.of('Australia/Melbourne')

    def setup() {
        subject = new DateTimeHelper('Melbourne', 145.0, -38.0)
    }

    def 'sunset calculated correctly for bare dates'() {
        when:
        ZonedDateTime sunset = subject.sunsetFor(yr, mn, dy, melbourneTZ)

        then:
        Math.abs(sunset.until(result, ChronoUnit.MINUTES)) < 5

        where:
        yr   | mn | dy | result
        2014 | 8  | 31 | ZonedDateTime.of(2014, 8, 31, 17, 57, 0, 0, melbourneTZ)
        2014 | 5  | 21 | ZonedDateTime.of(2014, 5, 21, 7, 14, 0, 0, ZoneOffset.UTC)
        2013 | 12 | 17 | ZonedDateTime.of(2013, 12, 17, 20, 39, 0, 0, melbourneTZ)
    }

    def 'percent of night calculates properly'() {
        expect:
        Math.abs(result - subject.percentOfNight(target)) < 2

        where:
        target                                            | result
        ZonedDateTime.of(2014, 12, 31, 19, 0, 0, 0, melbourneTZ) | 0
        ZonedDateTime.of(2015, 1, 1, 1, 23, 0, 0, melbourneTZ)   | 50
        ZonedDateTime.of(2015, 1, 1, 5, 59, 0, 0, melbourneTZ)   | 100
        ZonedDateTime.of(2015, 1, 1, 3, 40, 0, 0, melbourneTZ)   | 75
        ZonedDateTime.of(2014, 11, 10, 12,0,0, 0, melbourneTZ)   | 0
    }

    def 'is Moon up or not'() {
        expect:
        result == subject.isMoonUp(target)

        where:
        target                                             | result
        ZonedDateTime.of(2014, 12, 31, 18, 20, 0, 0, melbourneTZ) | true
        ZonedDateTime.of(2014, 12, 31, 8, 20, 0, 0, melbourneTZ)  | false
        ZonedDateTime.of(2015, 1, 6, 19, 5, 5, 0, melbourneTZ)    | false
        ZonedDateTime.of(2015, 1, 6, 22, 0, 0, 0, melbourneTZ)    | true
        ZonedDateTime.of(2015, 1, 7, 9, 0, 0, 0, melbourneTZ)     | false
    }

    def 'is Sun up or not'() {
        expect:
        result == subject.isSunUp(target)

        where:
        target                                            | result
        ZonedDateTime.of(2014, 12, 31, 12, 0, 0, 0, melbourneTZ) | true
        ZonedDateTime.of(2014, 12, 31, 21, 0, 5, 0, melbourneTZ) | false
        ZonedDateTime.of(2015, 1, 1, 4, 30, 30, 0, melbourneTZ)  | false
        ZonedDateTime.of(2015, 1, 1, 6, 3, 4, 0, melbourneTZ)    | true
    }

    def 'moonIllum calculated correctly'() {
        expect:
        result == subject.moonIllumPercent(target)

        where:
        target                                            | result
        ZonedDateTime.of(2014, 12, 31, 12, 0, 0, 0, melbourneTZ) | 0
        ZonedDateTime.of(2014, 12, 31, 22, 0, 0, 0, melbourneTZ) | 77
        ZonedDateTime.of(2015, 1, 5, 22, 0, 0, 0, melbourneTZ)   | 99

    }
}
