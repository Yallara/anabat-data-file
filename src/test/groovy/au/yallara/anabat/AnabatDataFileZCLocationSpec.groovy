/*
 * Copyright (c) 2014. Anne Jessel
 *
 * This file is part of AnabatDataFile.
 *
 * AnabatDataFile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AnabatDataFile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AnabatDataFile.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.anabat

class AnabatDataFileZCLocationSpec extends AnabatDataFileZCBaseSpec {

    def 'location parsed when no location present'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes

        when:
        adFile.parse(data)

        then:
        !adFile.location
    }

    def 'location parsed when present'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes

        when:
        adFile.parse(data)

        then:
        adFile.location == 'Cheltenham Golf Course and Park'
    }

    def 'changed location is written to byte array'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes

        when:
        adFile.parse(data)
        adFile.location = 'Somewhere'
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())

        then:
        newFile.location == 'Somewhere'
    }

    def 'changed location that is too long is truncated'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes
        def newLoc = '1234567890123456789012345678901234567890ABC'
        def expected = '1234567890123456789012345678901234567890'

        when:
        adFile.parse(data)
        adFile.location = newLoc
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())

        then:
        newFile.location == expected
    }

    def 'set to empty location clears field'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes

        when:
        adFile.parse(data)
        adFile.location = ''
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())

        then:
        newFile.location == ''
    }

    def 'set to null location clears field'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes

        when:
        adFile.parse(data)
        adFile.location = null
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())

        then:
        newFile.location == ''
    }

}
