/*
 * Copyright (c) 2014. Anne Jessel
 *
 * This file is part of AnabatDataFile.
 *
 * AnabatDataFile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AnabatDataFile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AnabatDataFile.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.anabat

class AnabatDataFileZCSpec extends AnabatDataFileZCBaseSpec {

    def 'parsed data object is not same as returned data object'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes

        when:
        adFile.parse(data)
        def result = adFile.asByteArray()

        then:
        !data.is(result)
    }

    def 'divRatio parsed correctly'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes

        when:
        adFile.parse(data)
        def result = adFile.divRatio

        then:
        result == 8
    }
    
    def 'notes parsed correctly'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes

        when:
        adFile.parse(data)
        def result = adFile.note

        then:
        result == 'V4019g'
    }

    def 'empty notes give empty string'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes

        when:
        adFile.parse(data)
        def result = adFile.note

        then:
        result == ''
    }

    def 'short note set properly'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes
        def newNote = 'This is a short note'

        when:
        adFile.parse(data)
        adFile.note = newNote
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())
        def found = newFile.note

        then:
        found == newNote
    }

    def 'long note is split and recombined properly'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes
        def newNote = 'This is a long note that will need to be split into two so it fits across two fields. We\'ll see if it works.'

        when:
        adFile.parse(data)
        adFile.note = newNote
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())
        def found = newFile.note

        then:
        found == newNote
    }

    def 'overly long note is truncated'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes
        def newNote = 'This is a long note that will need to be split into two so it fits across two fields.' +
                      'In fact, it is way too long. It will also be truncated, which means the last portion' +
                      ' will be lost. We\'ll see if it works.'
        def expected = 'This is a long note that will need to be split into two so it fits across two fields.' +
                       'In fact, it is way too long. It will also be truncated, which'

        when:
        adFile.parse(data)
        adFile.note = newNote
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())
        def found = newFile.note

        then:
        found == expected
    }

    def 'long note without spaces is handled properly'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes
        def newNote = '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
        def expected = '123456789012345678901234567890123456789012345678901234567890123456789012 3456789012345678901234567890'

        when:
        adFile.parse(data)
        adFile.note = newNote
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())
        def found = newFile.note

        then:
        found == expected
    }

    def 'null note is handled'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes
        def newNote = null

        when:
        adFile.parse(data)
        adFile.note = newNote
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())
        def found = newFile.note

        then:
        found == ''
    }

    def 'spec field is parsed'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes

        when:
        adFile.parse(data)

        then:
        adFile.spec == 'SD1 Mod1'
    }

    def 'spec field is written'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes
        def newSpec = 'A spec'

        when:
        adFile.parse(data)
        adFile.spec = newSpec
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())
        def found = newFile.spec

        then:
        found == newSpec
    }

    def 'temperature parsed correctly from Express detector'() {
        given:
        def data = this.getClass().getResource(expressFile1).bytes

        when:
        adFile.parse(data)
        def temp = adFile.temperature

        then:
        temp == 14.00
    }

    def 'temperature parsed correctly from zc file'() {
        given:
        def data = this.getClass().getResource(zcFile1).bytes

        when:
        adFile.parse(data)
        def temp = adFile.temperature

        then:
        temp == 18.25
    }

    def 'temperature parsed correctly from zc file with incomplete info'() {
        given:
        def data = this.getClass().getResource(zcFile3).bytes

        when:
        adFile.parse(data)
        def temp = adFile.temperature

        then:
        temp == 15.5
    }

    def 'temperature parsed correctly when not present'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes

        when:
        adFile.parse(data)
        def temp = adFile.temperature

        then:
        temp == null
    }

    def 'battery voltage parsed correctly from Express detector'() {
        given:
        def data = this.getClass().getResource(expressFile1).bytes

        when:
        adFile.parse(data)
        def volts = adFile.batteryVoltage

        then:
        volts == 6.155
    }

    def 'battery voltage parsed correctly from zc file'() {
        given:
        def data = this.getClass().getResource(zcFile1).bytes

        when:
        adFile.parse(data)
        def volts = adFile.batteryVoltage

        then:
        volts == 5.199
    }

    def 'battery voltage parsed correctly when not present'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes

        when:
        adFile.parse(data)
        def volts = adFile.batteryVoltage

        then:
        volts == null
    }

}
