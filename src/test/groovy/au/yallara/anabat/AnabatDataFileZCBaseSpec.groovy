/*
 * Copyright (c) 2014. Anne Jessel
 *
 * This file is part of AnabatDataFile.
 *
 * AnabatDataFile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AnabatDataFile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AnabatDataFile.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.anabat


import spock.lang.Specification

abstract class AnabatDataFileZCBaseSpec extends Specification {
    protected static final String noSpeciesFile = '/nc012021.03#'
    protected static final String twoSpeciesFile = '/nc012004.20#'
    protected static final String allFieldsFile = '/nc012000.48#'
    protected static final String expressFile1 = '/OB111950.54#'
    protected static final String zcFile1 = '/2024-02-15_19-51-32.zc'
    protected static final String zcFile2 = '/2024-02-15_19-51-53.zc'
    protected static final String zcFile3 = '/2024-03-05 00-15-11.zc'
    protected AnabatDataFileZC adFile

    def setup() {
        adFile = new AnabatDataFileZC()
    }
}
