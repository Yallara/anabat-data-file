/*
 * Copyright (c) 2014. Anne Jessel
 *
 * This file is part of AnabatDataFile.
 *
 * AnabatDataFile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AnabatDataFile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AnabatDataFile.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.anabat


import java.time.LocalDate
import java.time.ZoneOffset
import java.time.ZonedDateTime

class AnabatDataFileZCDateSpec extends AnabatDataFileZCBaseSpec {

    def 'date recorded parsed properly'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes
        def expected = ZonedDateTime.of(2013, 12, 1, 20, 0, 48, 7353000, ZoneOffset.UTC )

        when:
        adFile.parse(data)
        def dateRecorded = adFile.dateRecorded

        then:
        expected == dateRecorded
    }

    def 'header date parsed properly'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes
        def expected = LocalDate.of(2013,12,1)

        when:
        adFile.parse(data)
        def headerDate = adFile.headerDate

        then:
        expected == headerDate
    }

    def 'empty header date returns null'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes

        when:
        adFile.parse(data)
        def headerDate = adFile.headerDate

        then:
        headerDate == null
    }

    def 'set new HeaderDate from integers'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes
        def expected = LocalDate.of(2014, 2, 3)

        when:
        adFile.parse(data)
        adFile.setHeaderDate(2014,2,3)
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())
        def headerDate = newFile.headerDate

        then:
        headerDate == expected
    }

    def 'headerDate removed when set to null'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes

        when:
        adFile.parse(data)
        adFile.setHeaderDate(null)
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())
        def headerDate = newFile.headerDate

        then:
        headerDate == null
    }

}
