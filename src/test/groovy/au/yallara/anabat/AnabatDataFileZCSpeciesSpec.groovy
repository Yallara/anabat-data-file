/*
 * Copyright (c) 2014. Anne Jessel
 *
 * This file is part of AnabatDataFile.
 *
 * AnabatDataFile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AnabatDataFile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AnabatDataFile.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.anabat

class AnabatDataFileZCSpeciesSpec extends AnabatDataFileZCBaseSpec {

    def 'species list parsed properly with one species'() {

        given:
        def data = this.getClass().getResource(allFieldsFile).bytes

        when:
        adFile.parse(data)

        then:
        adFile.species == ['Cgould']
    }

    def 'species list parsed properly with no species'() {

        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes

        when:
        adFile.parse(data)

        then:
        adFile.species.isEmpty()
    }

    def 'species list parsed properly with two species'() {

        given:
        def data = this.getClass().getResource(twoSpeciesFile).bytes

        when:
        adFile.parse(data)

        then:
        adFile.species.sort() == ['Cgould', 'Vvult']
    }

    def 'add new species to existing list'() {

        given:
        def data = this.getClass().getResource(allFieldsFile).bytes

        when:
        adFile.parse(data)
        adFile.addSpecies('Vvult')
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())

        then:
        newFile.species.sort() == ['Cgould', 'Vvult']
    }

    def 'clear existing species list'() {
        given:
        def data = this.getClass().getResource(twoSpeciesFile).bytes

        when:
        adFile.parse(data)
        adFile.species = null
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())

        then:
        adFile.species == []
        newFile.species == []
    }

    def 'replace existing species list'() {
        given:
        def data = this.getClass().getResource(twoSpeciesFile).bytes
        def newSpecies = ['Mormo', 'Nycto']

        when:
        adFile.parse(data)
        adFile.species = newSpecies
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())

        then:
        newFile.species.sort() == newSpecies
    }

    def 'remove existing species'() {
        given:
        def data = this.getClass().getResource(twoSpeciesFile).bytes
        def expected = ['Vvult']

        when:
        adFile.parse(data)
        adFile.removeSpecies('Cgould')
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())

        then:
        newFile.species == expected
    }

    def 'remove species not listed makes no change or error'() {
        given:
        def data = this.getClass().getResource(twoSpeciesFile).bytes
        def expected = ['Cgould', 'Vvult']

        when:
        adFile.parse(data)
        adFile.removeSpecies('Mormo')
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())

        then:
        newFile.species.sort() == expected
    }

    def 'remove species not listed makes no change or error in zc file'() {
        given:
        def data = this.getClass().getResource(zcFile1).bytes
        def expected = ['Cgould']

        when:
        adFile.parse(data)
        adFile.removeSpecies('Mormo')
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())

        then:
        newFile.species.sort() == expected
    }


}
