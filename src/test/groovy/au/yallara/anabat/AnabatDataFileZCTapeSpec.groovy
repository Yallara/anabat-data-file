/*
 * Copyright (c) 2014. Anne Jessel
 *
 * This file is part of AnabatDataFile.
 *
 * AnabatDataFile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AnabatDataFile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AnabatDataFile.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.anabat

class AnabatDataFileZCTapeSpec extends AnabatDataFileZCBaseSpec {

    def 'tape info in data file parsed'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes

        when:
        adFile.parse(data)
        def tape = adFile.tape

        then:
        tape == 'SN 04382'
    }

    def 'parsing empty tape field gives empty String'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes

        when:
        adFile.parse(data)
        def tape = adFile.tape

        then:
        tape == ''
    }

    def 'changed tape is written to byte array'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes
        def value = 'ABC'

        when:
        adFile.parse(data)
        adFile.tape = value
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())

        then:
        newFile.tape == value

    }
}
