package au.yallara.anabat

import spock.lang.IgnoreIf

import java.time.ZoneId
import java.time.ZonedDateTime

/**
 * TODO Document this file
 * Created: 23/4/24
 * Copyright 2024
 * @author Coherent Software Australia Pty Ltd
 */
@IgnoreIf({ env.CI })
class AnabatDataFileWavSpec extends AnabatDataFileWavBaseSpec {

    static final ZoneId melbZone = ZoneId.of('Australia/Melbourne')

    def 'fields parsed properly for single species file'() {
        given:
        def dataFile = toDataFile(oneSpeciesFile)

        when:
        adFile.parse(dataFile)

        then:
        adFile.guanoVersion == '1.0'
        adFile.hardwareMake == 'Titley Scientific'
        adFile.hardwareModel == 'Chorus'
        adFile.firmwareVersion == 'Sonic v3.0.4.24052'
        adFile.hpFilter == new BigDecimal('10.00')
        adFile.microphone == 'Ultrasonic'
        adFile.moonAge == new BigDecimal('28.0')
        adFile.activation == 'Triggered'
        adFile.zcSensitivity == 16
        adFile.minTrigger == 10000
        adFile.maxTrigger == 250000
        adFile.minEvent == 2
        adFile.triggerWindow == 2000
        adFile.sdCardSerial == '3563-3831'
        adFile.getSpecies() ==~ ['Vvult']
        adFile.getTape() == '762427'
        adFile.getDateRecorded() == ZonedDateTime.of(2024, 3, 8, 23, 30, 57, 0, melbZone)
        adFile.getLocation() == 'Highett Grassy Woodland'
        adFile.getBatteryVoltage() == new BigDecimal('5.34')
        adFile.getLatitude() == new BigDecimal('-37.954086')
        adFile.getLongitude() == new BigDecimal('145.039810')
        adFile.getAltitude() == 47
        adFile.getNote() == null
        adFile.getTemperature() == new BigDecimal('35.25')
    }

    def 'species parsed properly for two species file'() {
        given:
        def dataFile = toDataFile(twoSpeciesFile)

        when:
        adFile.parse(dataFile)

        then:
        adFile.getSpecies() ==~ ['Cgould','Nyct']
    }

    def 'floating point altitude parsed correctly'() {
        given:
        def dataFile = toDataFile(twoSpeciesFile)

        when:
        adFile.parse(dataFile)

        then:
        adFile.getAltitude() == 2
    }

    def 'exception thrown if try to add species'() {
        given:
        def dataFile = toDataFile(otherFile)

        when:
        adFile.parse(dataFile)
        adFile.addSpecies('Cgould')

        then:
        thrown(UnsupportedOperationException)
    }

    def 'exception thrown if try to save file'() {
        given:
        def dataFile = toDataFile(otherFile)

        when:
        adFile.parse(dataFile)
        adFile.save(dataFile)

        then:
        thrown(UnsupportedOperationException)
    }

    def 'exception thrown if zc file specified'() {
        given:
        def datafile = toDataFile(zcFile1)

        when:
        adFile.parse(datafile)

        then:
        thrown(IOException)
    }

    def 'exception thrown if specified exiftool path does not exist'() {
        given:
        def anabat = new AnabatDataFileWav('/usr/bin/absent')
        def datafile = toDataFile(otherFile)

        when:
        anabat.parse(datafile)

        then:
        thrown(UnsupportedOperationException)
    }
}
