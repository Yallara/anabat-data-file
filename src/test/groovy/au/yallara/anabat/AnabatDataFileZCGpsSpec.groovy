/*
 * Copyright (c) 2014. Anne Jessel
 *
 * This file is part of AnabatDataFile.
 *
 * AnabatDataFile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AnabatDataFile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AnabatDataFile.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.anabat

class AnabatDataFileZCGpsSpec extends AnabatDataFileZCBaseSpec {

    def 'gps datum parsed correctly'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes

        when:
        adFile.parse(data)
        def datum = adFile.gpsDatum

        then:
        datum == 'WGS84'
    }

    def 'empty gps datum gives empty string'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes

        when:
        adFile.parse(data)
        def datum = adFile.gpsDatum

        then:
        datum == ''
    }

    def 'gops datum read from Express detector'() {
        given:
        def data = this.getClass().getResource(expressFile1).bytes

        when:
        adFile.parse(data)
        def datum = adFile.gpsDatum

        then:
        datum == 'WGS84'
    }

    def 'latitude parsed correctly'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes

        when:
        adFile.parse(data)
        def lat = adFile.latitude

        then:
        lat == -38.96511
    }

    def 'missing latitude gives null'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes

        when:
        adFile.parse(data)
        def lat = adFile.latitude

        then:
        lat == null
    }

    def 'latitude parsed correctly from Express detector'() {
        given:
        def data = this.getClass().getResource(expressFile1).bytes

        when:
        adFile.parse(data)
        def lat = adFile.latitude

        then:
        lat == -37.96918
    }

    def 'longitude parsed correctly'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes

        when:
        adFile.parse(data)
        def lng = adFile.longitude

        then:
        lng == 145.05103
    }

    def 'longitude parsed correctly from Express detector'() {
        given:
        def data = this.getClass().getResource(expressFile1).bytes

        when:
        adFile.parse(data)
        def lng = adFile.longitude

        then:
        lng == 145.05248
    }

    def 'longitude parsed correctly from zc file'() {
        given:
        def data = this.getClass().getResource(zcFile1).bytes

        when:
        adFile.parse(data)
        def lng = adFile.longitude

        then:
        lng == 145.02941
    }

    def 'missing longitude gives null'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes

        when:
        adFile.parse(data)
        def lng = adFile.longitude

        then:
        lng == null
    }

    def 'altitude parsed correctly'() {
        given:
        def data = this.getClass().getResource(allFieldsFile).bytes

        when:
        adFile.parse(data)
        def alt = adFile.altitude

        then:
        alt == 19
    }

    def 'missing altitude gives null'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes

        when:
        adFile.parse(data)
        def alt = adFile.altitude

        then:
        alt == null
    }

    def 'altitude parsed correctly from Express detector'() {
        given:
        def data = this.getClass().getResource(expressFile1).bytes

        when:
        adFile.parse(data)
        def alt = adFile.altitude

        then:
        alt == 41
    }

    def 'new south east gps coords set'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes
        BigDecimal latValue = -38.9765789
        BigDecimal longValue = 145.7656789

        when:
        adFile.parse(data)
        adFile.setLatLong('WGS', latValue, longValue)
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())
        def newLat = newFile.latitude
        def newLong = newFile.longitude
        def newDatum = newFile.gpsDatum

        then:
        newLat == -38.97657
        newLong == 145.76567
        newDatum == 'WGS'
    }

    def 'new north west latitude set'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes
        BigDecimal latValue = 38.9765789
        BigDecimal longValue = -34.7651234

        when:
        adFile.parse(data)
        adFile.setLatLong('DTM123', latValue, longValue)
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())
        def newLat = newFile.latitude
        def newLong = newFile.longitude
        def newDatum = newFile.gpsDatum

        then:
        newLat == 38.97657
        newLong == -34.76512
        newDatum == 'DTM123'
    }

    def 'latitude longitude is cleared'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes

        when:
        adFile.parse(data)
        adFile.setLatLong(null, null, null)
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())
        def newLat = newFile.latitude
        def newLong = newFile.longitude
        def newDatum = newFile.gpsDatum

        then:
        newLat == null
        newLong == null
        newDatum == ''
    }

    def 'altitude is set'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes
        def value = 99

        when:
        adFile.parse(data)
        adFile.setAltitude(value)
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())
        def newAlt = newFile.altitude

        then:
        newAlt == value
    }

    def 'out of range altitude throws exception'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes
        def value = 12341

        when:
        adFile.parse(data)
        adFile.setAltitude(value)
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())
        def newAlt = newFile.altitude

        then:
        thrown IllegalArgumentException
    }

    def 'negative altitude is set'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes
        def value = -991

        when:
        adFile.parse(data)
        adFile.setAltitude(value)
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())
        def newAlt = newFile.altitude

        then:
        newAlt == value
    }

    def 'altitude is cleared by setting to null'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes

        when:
        adFile.parse(data)
        adFile.setAltitude(null)
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())
        def newAlt = newFile.altitude

        then:
        newAlt == null
    }

    def 'zero altitude is set'() {
        given:
        def data = this.getClass().getResource(noSpeciesFile).bytes
        def value = 0

        when:
        adFile.parse(data)
        adFile.setAltitude(value)
        def newFile = new AnabatDataFileZC().parse(adFile.asByteArray())
        def newAlt = newFile.altitude

        then:
        newAlt == value
    }

}
