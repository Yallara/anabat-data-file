package au.yallara.anabat

import spock.lang.Specification

/**
 * TODO Document this file
 * Created: 23/4/24
 * Copyright 2024
 * @author Coherent Software Australia Pty Ltd
 */
class AnabatDataFileWavBaseSpec extends Specification {
    protected static final String oneSpeciesFile = '/762427_2024-03-08_23-30-57.wav'
    protected static final String twoSpeciesFile = '/762427_2024-03-08_23-30-42.wav'
    protected static final String otherFile = '/762427_2024-03-08_23-31-04.wav'
    protected static final String zcFile1 = '/2024-02-15_19-51-32.zc'

    protected AnabatDataFileWav adFile

    def setup() {
        adFile = new AnabatDataFileWav()
    }

    File toDataFile(String fname) {
        def cls = this.getClass()
        def rsc = cls.getResource(fname)
        def uri = rsc.toURI()
        return new File(uri)
    }
}
