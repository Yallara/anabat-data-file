/*
 * Copyright (c) 2014-2024. Yallara
 * This file is part of AnabatDataFile.
 *
 * AnabatDataFile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AnabatDataFile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AnabatDataFile.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.util

import au.yallara.astronomy.woomera.Moon
import au.yallara.astronomy.woomera.NamedObject
import au.yallara.astronomy.woomera.Sun
import au.yallara.astronomy.woomera.Times
import groovy.transform.AutoFinal
import groovy.transform.Memoized

import au.yallara.astronomy.woomera.Telescope

import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit

/**
 * Uses Woomera library to calculate data related to Sun and Moon for a specific location.
 *
 * NOT THREAD-SAFE!
 *
 * There are a lot of assumptions in this class, that should be okay for locations away from the poles,
 * and where passed datetimes are close to the location's local timezone.
 */
class DateTimeHelper {

    private final Telescope location
    private final Sun sun = new Sun()
    private final Moon moon = new Moon()
    private final ZoneId utcTZ = ZoneOffset.UTC

    /**
     * Initialise necessary objects for a specific location. Woomera's altitude will be set to 0.
     * @param locname The name of this location.
     * @param longitude The longitude.
     * @param latitude The latitude.
     */
    @AutoFinal
    DateTimeHelper(String locname, double longitude, double latitude) {
        location = new Telescope()
        location.setGeodetic(locname, Math.toRadians(longitude), Math.toRadians(latitude), 0)

    }

    /**
     * Calculate how far through the night is the passed datetime for the location represented by this object.
     *
     * A datetime exactly half way between local sunset and sunrise will return 50. A datetime between
     * sunrise and sunset but after midday will produce 0, whereas before midday it will produce 100.
     *
     * @param dateTime The datetime of interest.
     * @return The percent, as an Integer, of the night.
     */
    @AutoFinal
    Integer percentOfNight(ZonedDateTime dateTime) {

        ZonedDateTime sunset = nextSunSet(dateTime.minusDays(1))
        ZonedDateTime sunrise = nextSunRise(dateTime)

        long nightMins = sunset.until(sunrise, ChronoUnit.MINUTES)

        if (nightMins > (24 * 60)) {
            // passed datetime was during day
            return dateTime.hour >= 12 ? 0 : 100
        }
        def afterSunsetMins = sunset.until(dateTime, ChronoUnit.MINUTES)
        return (afterSunsetMins * 100.0 / nightMins) as Integer
    }

    /**
     * Calculate time of sunset for the supplied date at this location. This method is intended as a faster
     * option than if a full datetime were handled.
     *
     * Timezones are not handled well.
     *
     * @param year Year
     * @param month Month
     * @param day Day
     * @param zone Timezone: defaults to UTC
     * @return Datetime of sunset.
     */
    @AutoFinal
    ZonedDateTime sunsetFor(int year, int month, int day, ZoneId zone = utcTZ) {
        return sunSetRiseFor(year, month, day, false, zone)
    }

    /**
     * Calculate time of sunrise for the supplied date at this location.This method is intended as a faster
     * option than if a full datetime were handled.
     *
     * Timezones are not handled well.
     *
     * @param year Year
     * @param month Month
     * @param day Day
     * @param zone Timezone: defaults to UTC
     * @return Datetime of sunrise.
     */
    @AutoFinal
    ZonedDateTime sunriseFor(int year, int month, int day, ZoneId zone = utcTZ) {
        return sunSetRiseFor(year, month, day, true, zone)
    }

    // TODO This does not yet handle timezones nicely
    @Memoized
    @AutoFinal
    private ZonedDateTime sunSetRiseFor(int year, int month, int day, boolean forRise, ZoneId zone = utcTZ) {
        location.setUT(year, month, day, (forRise ? 2 : 14), 0, 0)
        sun.update(location)
        def result = sun.nextRiseSet(location, NamedObject.RISESUN, forRise)
        double[] resultTime = new double[3]
        result.getUThms(resultTime)
        ZonedDateTime sunRiseSet = ZonedDateTime.of(year, month, day, resultTime[0] as int, resultTime[1] as int, 0, 0, utcTZ)
        return zone == utcTZ ? sunRiseSet : sunRiseSet.withZoneSameInstant(zone)

    }

    /**
     * How much of the moon is illuminated at the given datetime.
     *
     * @param dateTime The datetime of interest.
     * @return The percentage illuminated. 0 if the moon is not above the horizon.
     */
    @AutoFinal
    Integer moonIllumPercent(ZonedDateTime dateTime) {
        if (!isMoonUp(dateTime)) {
            return 0
        }
        double[] moonInfo = getMoonInfo(dateTime)
        return (moonInfo[5] * 100) as Integer
    }

    /**
     * Is the Moon above the horizon at the passed datetime?
     *
     * @param dateTime The datetime of interest.
     * @return true or false.
     */
    @AutoFinal
    Boolean isMoonUp(ZonedDateTime dateTime) {
        ZonedDateTime nextRise = nextMoonRise(dateTime)
        ZonedDateTime nextSet = nextMoonSet(dateTime)
        return nextRise.isAfter(nextSet)
    }

    /**
     * Calculate the date and time of the next moonrise after the passed datetime.
     *
     * @param dateTime The starting datetime.
     * @return The next moon rise.
     */
    @AutoFinal
    ZonedDateTime nextMoonRise(ZonedDateTime dateTime) {
        return nextMoonRiseSet(dateTime, true)
    }

    /**
     * Calculate the date and time of the next moonset after the passed datetime.
     *
     * @param dateTime The starting datetime.
     * @return The next moon set.
     */
    @AutoFinal
    ZonedDateTime nextMoonSet(ZonedDateTime dateTime) {
        return nextMoonRiseSet(dateTime, false)
    }

    @AutoFinal
    private ZonedDateTime nextMoonRiseSet(ZonedDateTime dateTime, boolean forRise) {
        setLocationDateTime(dateTime)
        moon.update(location)
        def result = moon.nextRiseSet(location, NamedObject.RISESUN, forRise)
        return woomeraTimes2DateTime(result).withZoneSameInstant(dateTime.zone)
    }

    /**
     * Is the Sun above the horizon at the passed datetime?
     *
     * @param dateTime The datetime of interest.
     * @return true or false.
     */
    @AutoFinal
    Boolean isSunUp(ZonedDateTime dateTime) {
        ZonedDateTime nextRise = nextSunRise(dateTime)
        ZonedDateTime nextSet = nextSunSet(dateTime)
        return nextRise.isAfter(nextSet)
    }

    /**
     * Calculate the date and time of the next sunrise after the passed datetime.
     *
     * @param dateTime The starting datetime.
     * @return The next sun rise.
     */
    @AutoFinal
    ZonedDateTime nextSunRise(ZonedDateTime dateTime) {
        return nextSunRiseSet(dateTime, true)
    }

    /**
     * Calculate the date and time of the next sunset after the passed datetime.
     *
     * @param dateTime The starting datetime.
     * @return The next sun set.
     */
    @AutoFinal
    ZonedDateTime nextSunSet(ZonedDateTime dateTime) {
        return nextSunRiseSet(dateTime, false)
    }

    @AutoFinal
    private ZonedDateTime nextSunRiseSet(ZonedDateTime dateTime, boolean forRise) {
        setLocationDateTime(dateTime)
        sun.update(location)
        def result = sun.nextRiseSet(location, NamedObject.RISESUN, forRise)
        return woomeraTimes2DateTime(result).withZoneSameInstant(dateTime.zone)
    }

    @Memoized
    @AutoFinal
    private double[] getMoonInfo(ZonedDateTime dateTime) {
        setLocationDateTime(dateTime)
        moon.update(location)
        double[] moonInfo = new double[10]
        moon.getPhysics(moonInfo, location)
        return moonInfo
    }

    @AutoFinal
    private void setLocationDateTime(ZonedDateTime dateTime) {
        // dateTime includes timezone
        ZonedDateTime utc = dateTime.zone == utcTZ ? dateTime : dateTime.withZoneSameInstant(utcTZ)
        location.setUT(utc.year, utc.monthValue, utc.dayOfMonth, utc.hour, utc.minute, utc.second)
    }

    @Memoized
    @AutoFinal
    private ZonedDateTime woomeraTimes2DateTime(Times woomeraTimes) {
        double[] resultDate = new double[3]
        double[] resultTime = new double[3]
        woomeraTimes.getDate(resultDate)
        woomeraTimes.getUThms(resultTime)
        return ZonedDateTime.of(resultDate[0] as int, resultDate[1] as int, resultDate[2] as int,
                                resultTime[0] as int, resultTime[1] as int, resultTime[2] as int, 0, ZoneOffset.UTC)
    }

}
