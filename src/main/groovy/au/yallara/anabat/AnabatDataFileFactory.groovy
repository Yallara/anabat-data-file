/*
 * Copyright (c) 2024. Yallara
 * This file is part of AnabatDataFile.
 *
 * AnabatDataFile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AnabatDataFile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AnabatDataFile.  If not, see <http://www.gnu.org/licenses/>.
 */

package au.yallara.anabat

import org.jetbrains.annotations.Nullable


class AnabatDataFileFactory {

    /**
     * Create a AnabatDataFile for the specified file, configured with the passed Closure for timezone calculations.
     *
     * The calcTimeZone Closure defaults to UTC for .zc files, and Australia/Melbourne for .wav files.
     *
     * @param infile the Anabat file to load
     * @param calcTimeZone Optional Closure that returns a ZoneId for the timezone applicable to the data.
     *                     Passed one param: LocalDateTime
     * @return an object that implements the AnabatDataFile interface.
     */
    @Nullable
    static AnabatDataFile loadFile(File infile, Closure calcTimeZone = null) {
        AnabatDataFile anabat
        if (infile.name =~ /.+\.\d\d#/ || infile.name.endsWithIgnoreCase('.zc')) {
            anabat = new AnabatDataFileZC(calcTimeZone as Closure).parse(infile.bytes)
        } else if (infile.name.endsWithIgnoreCase('.wav')) {
            anabat = new AnabatDataFileWav(calcTimeZone as Closure).parse(infile)
        } else {
            anabat = null
        }
        return anabat
    }
}
