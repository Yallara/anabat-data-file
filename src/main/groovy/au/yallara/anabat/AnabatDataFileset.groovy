/*
 * Copyright (c) 2014, 2024 Yallara
 * This file is part of AnabatDataFile.
 *
 * AnabatDataFile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AnabatDataFile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AnabatDataFile.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package au.yallara.anabat

import groovy.io.FileType
import groovy.transform.AutoFinal

import java.time.ZoneId

/**
 * Contains a set of AnabatDataFile objects, and enables actions upon them.
 */
class AnabatDataFileset {

    private final List<File> fileset
    private final Closure<ZoneId> calcTimeZone
    private Closure filter

    /**
     * Defines a set of anabat data files under the passed directory.
     * @param fileNameList A list of filenames or File objects pointing to the anabat data files
     *                     or the top level of a directory tree of data files. If an item in the list is
     *                     the String '-', then a list of filenames or directory names will be read from System.in. This
     *                     allows the target filelist to be piped from another program.
     */
    @AutoFinal
    AnabatDataFileset(List fileNameList) {
        fileset = fileNameList.collect { item ->
            if (item instanceof String && item == '-') {
                // read from stdin
                List<File> files = []
                System.in.withReader {
                    def line = it.readLine()
                    while (line) {
                        files << new File(line)
                        line = it.readLine()
                    }
                }
                files
            } else {
                item instanceof File ? item : new File(item)
            }
        }.flatten() as List<File>
    }

    /**
     * Defines a set of anabat data files under the passed directory.
     *
     * @param fileNameList A list of filenames or File objects pointing to the anabat data files
     *                     or the top level of a directory tree of data files. If an item in the list is
     *                     the String '-', then a list of filenames or directory names will be read from System.in. This
     *                     allows the target filelist to be piped from another program.
     * @param setTimeZone A Closure that will return a String to use for the timezone.
     *                     Passed one param: LocalDateTime of the recorded datetime.
     */
    @AutoFinal
    AnabatDataFileset(List fileNameList, Closure<ZoneId> setTimeZone) {
        this(fileNameList)
        calcTimeZone = setTimeZone
    }

    /**
     * When processing this Fileset, only include those files that match the passed filter.
     *
     * The filter Closure will be passed an AnabatDataFile. The Closure should
     * return either true or false, according to whether this AnabatDataFile should
     * be processed.
     *
     * @param filt Closure returning true or false.
     */
    @AutoFinal
    void applyFilter(Closure filt) {
        filter = filt
    }

    /**
     * Remove a previously added filter.
     *
     * See applyFilter(Closure).
     */
    void clearFilter() {
        filter = null
    }

    /**
     * Recursively visits every anabat data file in this fileset,
     * and passes it (as an AnabatDataFile object) into the passed closure.
     *
     * If this fileset has a filter, added via applyFilter(Closure), only anabat data files
     * passing that filter will be passed into the action Closure.
     *
     * @param action A closure to be executed for each anabat data file. Passed
     * two params: the AnabatDataFile object, plus the corresponding File object.
     */
    void each(Closure action) {
        fileset.each { file ->
            if (file.isDirectory()) {
                file.eachFileRecurse(FileType.FILES) { File infile ->
                    actOnFileWithSave(infile, action, false)
                }
            } else if (file.isFile()) {
                actOnFileWithSave(file, action, false)
            }
        }
    }

    /**
     * Recursively visits every anabat data file in this fileset,
     * and passes it (as an AnabatDataFile object) into the passed closure. Once
     * the closure completes, overwrites the original data file with the possibly changed AnabatDataFile data.
     * <p>
     * <b>If the file type does not have writable metadata, the file will NOT be updated, silently.</b> Behaviour will
     * be the same as each(Closure).
     * <p>
     * If this fileset has a filter, added via applyFilter(Closure), only anabat data files
     * passing that filter will be passed into the action Closure.
     *
     * @param action A closure to be executed for each anabat data file. Passed
     * two params: the AnabatDataFile object, plus the corresponding File object.
     */
    void eachWithSave(Closure action) {
        fileset.each { file ->
            if (file.isDirectory()) {
                file.eachFileRecurse(FileType.FILES) { File infile ->
                    actOnFileWithSave(infile, action)
                }
            } else if (file.isFile()) {
                actOnFileWithSave(file, action)
            }
        }
    }

    @AutoFinal
    private void actOnFileWithSave(File infile, Closure action, Boolean saveIt = true) {
        AnabatDataFile anabat = AnabatDataFileFactory.loadFile(infile)
        if (anabat) {
            Boolean enable = filter ? filter.call(anabat) : true
            if (enable) {
                action.call(anabat, infile)
                if (saveIt && anabat.isWritable()) {
                    anabat.save(infile)
                }
            }
        }
    }

    @AutoFinal
    Object inject(Object collector, Closure action) {
        fileset.each { file ->
            if (file.isDirectory()) {
                file.eachFileRecurse(FileType.FILES) { File infile ->
                    actOnFileWithInject(collector, infile, action)
                }
            } else if (file.isFile()) {
                actOnFileWithInject(collector, file, action)
            }

        }
        return collector
    }

    private Object actOnFileWithInject(Object collector, File infile, Closure action) {
        AnabatDataFile anabat = AnabatDataFileFactory.loadFile(infile)
        if (anabat) {
            Boolean enable = filter ? filter.call(anabat) : true
            if (enable) {
                action.call(collector, anabat, infile)
            }
        }
        return collector
    }
}
