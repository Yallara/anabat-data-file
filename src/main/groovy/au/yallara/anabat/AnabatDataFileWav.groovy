/*
 * Copyright (c) 2024. Yallara
 * This file is part of AnabatDataFile.
 *
 * AnabatDataFile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AnabatDataFile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AnabatDataFile.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.anabat

import groovy.util.logging.Slf4j
import org.jetbrains.annotations.Nullable

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime

/**
 * Represents a .wav Anabat file. Read-only.
 */
@Slf4j
class AnabatDataFileWav extends AnabatDataFileAbstract {
    // fields not in .zc file
    private String guanoVersion
    private String hardwareMake
    private String hardwareModel
    private String firmwareVersion
    private BigDecimal hpFilter
    private BigDecimal moonAge
    private String microphone
    private String activation
    private Integer zcSensitivity
    private Integer minTrigger
    private Integer maxTrigger
    private Integer minEvent
    private Integer triggerWindow
    private String sdCardSerial

    // need external exiftool binary (version 12.63+) to read GUANO format metadata from .wav.
    private static final String exiftoolDefault = '/usr/bin/exiftool'
    private final String exiftool

    /**
     * Constructor.
     *
     * @param calcTimeZone Optional Closure that returns a ZoneId for the timezone applicable to the data.
     *                     Passed one param: LocalDateTime
     *                     Default version always returns Australia/Melbourne.
     */
    AnabatDataFileWav(Closure calcTimeZone = null) {
        super(calcTimeZone ?: { LocalDateTime ldt -> ZoneId.of('Australia/Melbourne') })
        exiftool = exiftoolDefault
    }

    /**
     * Constructor.
     *
     * @param String with absolute path to exiftool executable. MUST BE AT LEAST version 12.63.
     * @param calcTimeZone Optional Closure that returns a ZoneId for the timezone applicable to the data.
     *                     Passed one param: LocalDateTime
     *                     Default version always returns Australia/Melbourne.
     */
    AnabatDataFileWav(String exiftoolOverride, Closure calcTimeZone = null) {
        super(calcTimeZone ?: { LocalDateTime ldt -> ZoneId.of('Australia/Melbourne') })
        exiftool = exiftoolOverride ?: exiftoolDefault
    }

    /**
     * Return version of Guano format.
     *
     * @return Guano format version
     */
    String getGuanoVersion() {
        return guanoVersion
    }

    String getHardwareMake() {
        return hardwareMake
    }

    String getHardwareModel() {
        return hardwareModel
    }

    String getFirmwareVersion() {
        return firmwareVersion
    }

    /**
     * Setting of the High Pass filter.
     *
     * @return the frequency below which data is filtered out
     */
    BigDecimal getHpFilter() {
        return hpFilter
    }

    /**
     * How illuminated the Moon, as a percentage.
     *
     * @return percent of Moon illuminated.
     */
    BigDecimal getMoonAge() {
        return moonAge
    }

    /**
     * What type of microphone was used.
     *
     * For bat data, this will be Ultrasonic.
     *
     * @return microphone type
     */
    String getMicrophone() {
        return microphone
    }

    /**
     * How was the recording activated? Was it Triggered by specific sound, or was it
     * continuous or scheduled or...
     *
     * @return the method of activation
     */
    String getActivation() {
        return activation
    }

    Integer getZcSensitivity() {
        return zcSensitivity
    }

    /**
     * What was the lowest frequency that would trigger a recording, if activation was set to trigger?
     *
     * @return the lowest trigger frequency, in Hz.
     */
    Integer getMinTrigger() {
        return minTrigger
    }

    /**
     * What was the highest frequency that would trigger a recording, if activation was set to trigger?
     *
     * @return the highest trigger frequency, in Hz.
     */
    Integer getMaxTrigger() {
        return maxTrigger
    }

    Integer getMinEvent() {
        return minEvent
    }

    Integer getTriggerWindow() {
        return triggerWindow
    }

    /**
     * The serial number of the SD card.
     *
     * @return the SD card serial.
     */
    String getSdCardSerial() {
        return sdCardSerial
    }

    /**
     * Parse the passed .wav file, extract the metadata.
     *
     * Must be a .wav file with Guano metadata that exiftool can parse.
     *
     * @param infile the file to parse.
     * @return the object containing the metadata
     * @throws UnsupportedOperationException if exiftool is not executable
     * @throws IOException if exiftool reports an error, or does not report any Guano metadata
     */
    AnabatDataFile parse(File infile) throws UnsupportedOperationException, IOException {
        // first confirm exiftool is available
        Boolean haveExiftool = new File(exiftool).canExecute()
        if (!haveExiftool) {
            throw new UnsupportedOperationException("Executable exiftool not found as $exiftool, cannot process wav ${infile.name}")
        }
        String cmdline = "$exiftool -b -guano ${infile.absolutePath}"

        try {
            Process process = Runtime.runtime.exec(cmdline)
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.inputStream))

            List<String> rawGuano = reader.readLines()

            reader.close()

            if (!rawGuano) {
                throw new IOException('Exiftool did not provide any output')
            }

            if (rawGuano[0].startsWith('Error')) {
                throw new IOException("Exiftool failed with message ${rawGuano[0]}")
            }

            parseGuano(rawGuano)

        } catch (IOException ioe) {
            throw new IOException("Error reading metadata from ${infile.name}", ioe)
        }

        return this
    }

    /**
     * Not supported for .wav files.
     *
     * @throws UnsupportedOperationException
     */
    @Override
    protected void writeSpeciesList() throws UnsupportedOperationException {
        notWritable()
    }

    private void notWritable() {
        throw new UnsupportedOperationException('Guano metadata is not writable')
    }

    /**
     * Not supported for .wav files.
     *
     * @throws UnsupportedOperationException
     */
    @Override
    void setLatLong(@Nullable String datum, @Nullable BigDecimal lat, @Nullable BigDecimal lng) throws UnsupportedOperationException {
        notWritable()
    }

    /**
     * Not supported for .wav files.
     *
     * @throws UnsupportedOperationException
     */
    @Override
    void setAltitude(@Nullable Integer alt) throws IllegalArgumentException, UnsupportedOperationException {
        notWritable()
    }

    /**
     * Not supported for .wav files.
     *
     * @throws UnsupportedOperationException
     */
    @Override
    void setLocation(@Nullable String loc) throws UnsupportedOperationException {
        notWritable()
    }

    /**
     * Not supported for .wav files.
     *
     * @throws UnsupportedOperationException
     */
    @Override
    void setSpec(@Nullable String specStr) throws UnsupportedOperationException {
        notWritable()
    }

    /**
     * Not supported for .wav files.
     *
     * @throws UnsupportedOperationException
     */
    @Override
    void setTape(@Nullable String tapeStr) throws UnsupportedOperationException {
        notWritable()
    }

    /**
     * Not supported for .wav files.
     *
     * @throws UnsupportedOperationException
     */
    @Override
    void setGpsDatum(@Nullable String datum) throws UnsupportedOperationException {
        notWritable()
    }

    /**
     * Not supported for .wav files.
     *
     * @throws UnsupportedOperationException
     */
    @Override
    void setHeaderDate(@Nullable LocalDate newDate) throws UnsupportedOperationException {
        notWritable()
    }

    /**
     * Not supported for .wav files.
     *
     * @throws UnsupportedOperationException
     */
    @Override
    void setNote(@Nullable String newNote) throws UnsupportedOperationException {
        notWritable()
    }

    /**
     * Not supported for .wav files.
     *
     * @throws UnsupportedOperationException
     */
    @Override
    void save(File outfile) throws UnsupportedOperationException {
        notWritable()
    }

    private void parseGuano(List<String> guanoList) {
        Map<String, String> guanoMap = guanoList.collectEntries { line ->
            if (!line.isBlank()) {
                def vars = line.split(':', 2)
                [(vars[0]): vars[1]]
            }
        }
        guanoMap.each { key, val ->
            switch (key) {
                case 'GUANO|Version':
                    guanoVersion = val
                    break
                case 'Timestamp':
                    def localdate = LocalDateTime.parse(val)
                    ZoneId zone = calcTimeZone.call(localdate)
                    dtRecorded = ZonedDateTime.of(localdate, zone)
                    break
                case 'Species Manual ID':
                    spcLst = val.split(',')*.trim()
                    break
                case 'Make':
                    hardwareMake = val
                    break
                case 'Model':
                    hardwareModel = val
                    break
                case 'Firmware Version':
                    firmwareVersion = val
                    break
                case 'Serial':
                    tp = val
                    break
                case 'Loc Position':
                    def coords = val.split(' ')*.trim()
                    lat = new BigDecimal(coords[0])
                    lng = new BigDecimal(coords[1])
                    break
                case 'Loc Elevation':
                    def alttemp = new BigDecimal(val)
                    alti = alttemp.intValue()
                    break
                case 'Temperature Int':
                    temprtr = new BigDecimal(val)
                    break
                case 'Filter HP':
                    hpFilter = new BigDecimal(val)
                    break
                case 'Anabat|Battery voltage':
                    batVoltage = new BigDecimal(val)
                    break
                case 'Anabat|Moon Age':
                    moonAge = new BigDecimal(val)
                    break
                case 'Anabat|Microphone':
                    microphone = val
                    break
                case 'Anabat|Activation':
                    activation = val
                    break
                case 'Anabat|Zc Sensitivity':
                    zcSensitivity = Integer.parseInt(val)
                    break
                case 'Anabat|Trigger min freq':
                    minTrigger = Integer.parseInt(val)
                    break
                case 'Anabat|Trigger max freq':
                    maxTrigger = Integer.parseInt(val)
                    break
                case 'Anabat|Min event':
                    minEvent = Integer.parseInt(val)
                    break
                case 'Anabat|Trigger Window':
                    triggerWindow = Integer.parseInt(val)
                    break
                case 'Anabat|SD Card Serial':
                    sdCardSerial = val
                    break
                case 'Anabat|Place Names':
                    locn = val
                    break
                case 'Anabat|Start':
                case 'Anabat|Maximum File Duration':
                case 'Anabat|Places Names':
                    // ignore
                    break
                default:
                    log.info("Unrecognised GUANO metadata - $key : $val")
            }
        }
    }

    /**
     * Return a simple String representation of all metadata, suitable for debugging.
     *
     * @return formatted String containing all metadata.
     */
    String toString() {
        return """
GUANO version: ${getGuanoVersion()}
Timestamp: ${getDateRecorded()}
Species: ${getSpecies().join(', ')}
Make: ${getHardwareMake()}
Model: ${getHardwareModel()}
Firmware: ${getFirmwareVersion()}
Serial: ${getTape()}
Latitude: ${getLatitude()}
Longitude: ${getLongitude()}
Altitude: ${getAltitude()}
Temperature: ${getTemperature()}
Filter: ${getHpFilter()}
Voltage: ${getBatteryVoltage()}
Moon age: ${getMoonAge()}
Microphone: ${getMicrophone()}
Activation: ${getActivation()}
Zc Sensitivity: ${getZcSensitivity()}
Trigger Min Freq: ${getMinTrigger()}
Trigger Max Freq: ${getMaxTrigger()}
Min event: ${getMinEvent()}
Trigger Window: ${getTriggerWindow()}
SD Card Serial: ${getSdCardSerial()}
Location: ${getLocation()}
"""
    }

    /**
     * Standard equals, based on datetime recorded and tape (serial number).
     *
     * @param o object to compare
     * @return whether the two objects have the same datetime recorded and device serial number
     */
    @Override
    boolean equals(o) {
        if (this.is(o)) {
            return true
        }
        if (!(o instanceof AnabatDataFileWav)) {
            return false
        }

        AnabatDataFileWav that = (AnabatDataFileWav) o

        if (getDateRecorded() != that.getDateRecorded()) {
            return false
        }
        if (getTape() != that.getTape()) {
            return false
        }

        return true
    }

    /**
     * Generate hashcode based on date recorded and device serial number.
     *
     * @return the hashcode
     */
    @Override
    int hashCode() {
        int result
        result = getDateRecorded().hashCode()
        result = 31 * result + getTape().hashCode()
        return result
    }

    /**
     * This file format is read-only.
     *
     * @return false
     */
    @Override
    Boolean isWritable() {
        return false
    }
}
