/*
 * Copyright (c) 2014-2024. Yallara
 * This file is part of AnabatDataFile.
 *
 * AnabatDataFile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AnabatDataFile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AnabatDataFile.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.anabat

import groovy.transform.AutoFinal
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

/**
 * Represents a single .zc or .wav Anabat file. Allows read of both metadata formats, and write of .zc metadata.
 */
interface AnabatDataFile {
    @NotNull
    List<String> getSpecies()

    @Nullable
    ZonedDateTime getDateRecorded()

    @Nullable
    Integer getDivRatio()

    /**
     * Replace any existing species list with the passed one.
     *
     * @param speciesList The new list of species
     * @throws UnsupportedOperationException if file format is read-only (.wav)
     */
    @AutoFinal
    void setSpecies(@Nullable List<String> speciesList) throws UnsupportedOperationException

    /**
     * Add a single species to the existing list.
     *
     * @param newSpecies The new species to add.
     * @throws UnsupportedOperationException if file format is read-only (.wav)
     */
    @AutoFinal
    void addSpecies(@NotNull String newSpecies) throws UnsupportedOperationException

    /**
     * Remove the specified species from the current species list. If the passed species
     * is not there, do nothing.
     *
     * @param oldSpecies Species to remove.
     * @throws UnsupportedOperationException if file format is read-only (.wav)
     */
    @AutoFinal
    void removeSpecies(@NotNull String oldSpecies) throws UnsupportedOperationException

    /**
     * Set GPS coordinates.
     * @param datum The datum. Maximum 10 characters. Used in .zc only.
     * @param lat The latitude in decimal degrees, negative for South.
     * @param lng The longitude in decimal degrees, negative for West.
     * @throws UnsupportedOperationException if file format is read-only (.wav)
     */
    @AutoFinal
    void setLatLong(@Nullable String datum, @Nullable BigDecimal lat, @Nullable BigDecimal lng) throws UnsupportedOperationException

    /**
     * Latitude read from data file.
     *
     * @return Decimal latitude, negative for South
     */
    @Nullable
    BigDecimal getLatitude()

    /**
     * Longitude read from data file.
     *
     * @return Decimal longitude, negative for West
     */
    @Nullable
    BigDecimal getLongitude()

    /**
     * GPS Datum for lat/long. Not relevant for .wav.
     *
     * @return The datum name.
     */
    @Nullable
    String getGpsDatum()

    /**
     * Set the altitude in metres. Range must be between -999 and 9999.
     *
     * @param alt The new altitude.
     * @throws UnsupportedOperationException if file format is read-only (.wav)
     * @throws IllegalArgumentException if altitude is outside allowed range.
     */
    @AutoFinal
    abstract void setAltitude(@Nullable Integer alt) throws IllegalArgumentException, UnsupportedOperationException

    /**
     * Returns altitude, in metres, read from metadata.
     *
     * @return altitude in metres.
     */
    Integer getAltitude()

    /**
     * Set the location name. Maximum 40 characters.
     * @param loc The location name.
     * @throws UnsupportedOperationException if file format is read-only (.wav)
     */
    @AutoFinal
    void setLocation(@Nullable String loc) throws UnsupportedOperationException

    /**
     * Return location string from metadata.
     *
     * Corresponds to 'Place Names' in .wav.
     *
     * @return the location, if set.
     */
    @AutoFinal
    @Nullable
    String getLocation()

    @AutoFinal
    void setSpec(@Nullable String specStr)

    @Nullable
    String getSpec()

    /**
     * Set the serial number of the device, labelled 'tape' in .zc files.
     *
     * @param tapeStr the serial number to set
     * @throws UnsupportedOperationException if file format is read-only (.wav)
     */
    void setTape(@Nullable String tapeStr) throws UnsupportedOperationException

    /**
     * Return the serial number of the device.
     *
     * @return device serial number
     */
    @Nullable
    String getTape()

    /**
     * Set the GPS datum. .zc files only.
     *
     * @param datum
     * @throws UnsupportedOperationException if file format is read-only (.wav)
     */
    void setGpsDatum(@Nullable String datum) throws UnsupportedOperationException

    /**
     * Get the temperature recorded by the device.
     *
     * @return the temperature, in Celsius.
     */
    @Nullable
    BigDecimal getTemperature()

    /**
     * Get the battery voltage, in Volts.
     *
     * @return battery voltage
     */
    @Nullable
    BigDecimal getBatteryVoltage()

    /**
     * Return any notes within the metadata - .zc only.
     *
     * @return the notes.
     */
    @Nullable
    String getNote()

    /**
     * Set the text version of the date in the header. This is different to the dateRecorded,
     * which is set by the Anabat. .zc files only.
     *
     * @param year The year
     * @param month The month
     * @param day The day
     * @throws UnsupportedOperationException if file format is read-only (.wav)
     */
    @AutoFinal
    void setHeaderDate(Integer year, Integer month, Integer day) throws UnsupportedOperationException

    /**
     * Set the text version of the date in the header. This is different to the dateRecorded,
     * which is set by the Anabat. .zc files only.
     *
     * @param newDate The date to be set
     * @throws UnsupportedOperationException if file format is read-only (.wav)
     */
    @AutoFinal
    void setHeaderDate(@Nullable LocalDate newDate) throws UnsupportedOperationException

    /**
     * The date in the header of the file, which may not be the same as the date recoroded. - .zc files only.
     *
     * @return The date in the header, as a LocalDate.
     */
    @Nullable
    LocalDate getHeaderDate()

    /**
     * Set the note field. If more than 73 characters, will be stored in two fields. The first is 73 chars, the second 80.
     * The note will be split at the space char before the 73 chars. If there is no suitable space char,
     * will be split at 73 chars. If the amount remaining after the split is more than 80 chars, it will be
     * truncated.
     *
     * .zc files only.
     *
     * @param newNote The new note.
     * @throws UnsupportedOperationException if file format is read-only (.wav)
     */
    void setNote(@Nullable String newNote) throws UnsupportedOperationException

    /**
     * Save the metadata to a file. .zc files only.
     *
     * @param outfile the file to write to
     * @throws UnsupportedOperationException if file format is read-only (.wav)
     */
    void save(File outfile) throws UnsupportedOperationException

    /**
     * Returns whether this file format is writable.
     *
     * @return whether writable
     */
    Boolean isWritable()
}
