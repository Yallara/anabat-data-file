/*
 * Copyright (c) 2014, 2024. Yallara
 * This file is part of AnabatDataFile.
 *
 * AnabatDataFile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AnabatDataFile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AnabatDataFile.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.anabat

import groovy.transform.AutoFinal
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable

import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime

/**
 * Represents a .zc Anabat file. Reads and writes the metadata.
 */
class AnabatDataFileZC extends AnabatDataFileAbstract {

    // all lengths in bytes
    private static final Integer locationMaxSize = 40
    private static final Integer speciesMaxSize = 50
    private static final Integer specMaxSize = 16
    private static final Integer noteMaxSize = 73
    private static final Integer note1MaxSize = 80
    private static final Integer tapeMaxSize = 8
    private static final Integer gpsDatumMaxSize = 10

    private static final Integer dataFileVersion = 132

    private static final Integer textHeaderOffset = 0x06

    private byte[] rawData

    /**
     * Constructor.
     * @param calcTimeZone Optional Closure that returns a ZoneId for the timezone applicable to the data.
     *                     Passed one param: LocalDateTime
     *                     Default version always returns UTC.
     */
    @AutoFinal
    AnabatDataFileZC(Closure calcTimeZone = null) {
        super(calcTimeZone)
    }

    /**
     * Parse the passed anabat data, ready for use by the remaining methods.
     *
     * Takes a copy of the data.
     *
     * Given the path to an anabat data file, parsing could be implemented in Groovy thus:
     *
     * <code>
     *     byte[] data = new File(filename).bytes
     *     AnabatDataFile anabatData = new AnabatDataFile()
     *     anabatData.parse(data)
     * </code>
     *
     * @param data An anabat data file, as a byte array.
     * @throws UnsupportedOperationException if passed byte array is an unsupported format version.
     */
    @NotNull
    @AutoFinal
    AnabatDataFile parse(@NotNull byte[] data) throws UnsupportedOperationException {
        Integer version = fileVersion(data)
        if (version != dataFileVersion) {
            throw new UnsupportedOperationException("Data file version ${version} not supported")
        }

        rawData = data.clone() as byte[]

        spcLst = readSpecies()
        locn = readLocation()
        tp = readTape()
        divRatio = data[0x011e]
        gpsDtm = fetchHeaderStringField(0x0130, 10)
        lat = readLatitude()
        lng = readLongitude()
        alti = readAltitude()
        nte = readNote()
        dtRecorded = readDateRecorded()
        headDate = readHeaderDate()
        specHd = readHeaderSpec()

        if (nte) {
            // S=  63   Vbat=  6.026   T(C)=   10.75
            def matcher = (nte =~ /T\(C\)=\s+([0-9\.]+)/)
            if (matcher.find()) {
                def tempStr = matcher[0][1]
                temprtr = new BigDecimal(tempStr)
            }
            matcher = (nte =~ /Vbat=\s+([0-9\.]+)/)
            if (matcher.find()) {
                def batStr = matcher[0][1]
                batVoltage = new BigDecimal(batStr)
            }
        }
        return this
    }

    /**
     * Fetch the current version of this anabat data file as a byte array, suitable for writing to a file.
     *
     * @return The anabat data. Will return null if no data has been parsed into this object.
     */
    @Nullable
    private byte[] asByteArray() {
        return rawData
    }

    @NotNull
    @AutoFinal
    protected Integer fileVersion(@NotNull byte[] dataArray) {
        bytesToInt(dataArray[3])
    }

    @NotNull
    protected List<String> readSpecies() {
        String speciesRaw = fetchHeaderStringField(textHeaderOffset + 56, speciesMaxSize)
        return speciesRaw ? speciesRaw.split(',')*.trim().asList().findAll { it } : []
    }

    /**
     * Writes species list to metadata.
     */
    @Override
    protected void writeSpeciesList() {
        def speciesStr = species.join(',')
        storeStringData(checkStringLength(speciesStr, speciesMaxSize), textHeaderOffset + 56, speciesMaxSize)
    }

    @NotNull
    protected String readLocation() {
        fetchHeaderStringField(textHeaderOffset + 16, locationMaxSize)
    }

    @NotNull
    @AutoFinal
    protected String fetchHeaderStringField(@NotNull Integer offset, @NotNull Integer length) {
        byte[] data = fetchHeaderField(offset, length)
        new String(data).trim()
    }

    @NotNull
    @AutoFinal
    protected byte[] fetchHeaderField(@NotNull Integer offset, @NotNull Integer length) {
        Integer end = offset + length - 1
        rawData[offset..end] as byte[]
    }

    @NotNull
    protected String readTape() {
        fetchHeaderStringField(textHeaderOffset, tapeMaxSize)
    }

    @Nullable
    protected BigDecimal readLatitude() throws UnsupportedOperationException {
        def ns = fetchHeaderField(0x013a, 1)[0] as byte

        if (ns == 0x00) {
            return null
        }

        if (ns != 'S' && ns != 'N') {
            throw new UnsupportedOperationException('Unsupported positioning data detected. Only Lat/Long supported.')
        }

        def degrees = fetchHeaderStringField(0x013b, 2)
        def decimals = fetchHeaderStringField(0x013d, 5)
        def latStr1 = ns == 'S' ? '-' : ''
        def latStr = "${latStr1}${degrees}.${decimals}"
        def result = new BigDecimal(latStr)

        return result
    }

    @AutoFinal
    private void setLatitude(@Nullable BigDecimal lati) {
        lat = lati
        String hemisphere
        String degrees
        String decimals
        if (lat) {
            hemisphere = lat.signum() > 0 ? 'N' : 'S'
            Integer degreesInt = lat.abs().intValue()
            degrees = degreesInt.toString()
            decimals = ((lat.abs() - degreesInt) * 100000).intValue().toString()
            storeData(hemisphere.bytes, 0x013a, 1)
            storeData(degrees.bytes, 0x013b, 2)
            storeData(decimals.bytes, 0x013d, 5)
        } else {
            storeData(null, 0x013a, 8)
        }
    }

    @Nullable
    protected BigDecimal readLongitude() throws UnsupportedOperationException {
        def we = fetchHeaderField(0x0143, 1)[0] as Character

        if (we == '\u0000') {
            return null
        }

        if (we != 'W' && we != 'E') {
            throw new UnsupportedOperationException('Unsupported positioning data detected. Only Lat/Long supported.')
        }

        def degrees = fetchHeaderStringField(0x0144, 3)
        def decimals = fetchHeaderStringField(0x0147, 5)
        def longStr1 = we == 'W' ? '-' : ''
        def longStr = "${longStr1}${degrees}.${decimals}"
        def result = new BigDecimal(longStr)

        return result
    }

    @AutoFinal
    private void setLongitude(@Nullable BigDecimal lngtd) {
        lng = lngtd
        String hemisphere
        String degrees
        String decimals
        if (lng) {
            hemisphere = lng.signum() > 0 ? 'E' : 'W'
            Integer degreesInt = lng.abs().intValue()
            degrees = degreesInt.toString()
            decimals = ((lng.abs() - degreesInt) * 100000).intValue().toString()
            storeData(hemisphere.bytes, 0x0143, 1)
            storeData(degrees.bytes, 0x0144, 3)
            storeData(decimals.bytes, 0x0147, 5)
        } else {
            storeData(null, 0x0143, 9)
        }
    }

    /**
     * Set GPS coordinates.
     * @param datum The datum. Maximum 10 characters.
     * @param lat The latitude in decimal degrees, negative for South.
     * @param lng The longitude in decimal degrees, negative for West.
     */
    @AutoFinal
    void setLatLong(@Nullable String datum, @Nullable BigDecimal lat, @Nullable BigDecimal lng) {
        setGpsDatum(datum)
        setLatitude(lat)
        setLongitude(lng)
        rawData[0x0142] = ((lat && lng) ? 0x20 : 0x00) as byte
    }

    @Nullable
    protected Integer readAltitude() {
        def altStr = fetchHeaderStringField(0x014c, 4)

        if (!altStr) {
            return null
        }

        altStr.toInteger()
    }

    /**
     * Set the altitude in metres. Range must be between -999 and 9999.
     *
     * @param alt The new altitude.
     * @throws IllegalArgumentException if altitude is outside allowed range.
     */
    @AutoFinal
    void setAltitude(@Nullable Integer alt) throws IllegalArgumentException {
        if (alt == null) {
            storeData(null, 0x014c, 4)
        } else if (alt < MIN_ALTITUDE || alt > MAX_ALTITUDE) {
            throw new IllegalArgumentException("Altitude must be between $MIN_ALTITUDE and $MAX_ALTITUDE metres")
        } else {
            byte[] altBytes = alt.toString().padLeft(4).bytes

            storeData(altBytes, 0x014c, 4)
        }
        alti = alt
    }

    @NotNull
    protected String readNote() {
        def part1 = fetchHeaderStringField(textHeaderOffset + 122, 73)
        def part2 = fetchHeaderStringField(textHeaderOffset + 122 + 73, 80)

        // below may ad a space that wasn't originally there
        // however padding on write and trimming on read makes other options tricky
        "${part1} ${part2}".trim()
    }

    @NotNull
    protected String readHeaderSpec() {
        fetchHeaderStringField(textHeaderOffset + 106, specMaxSize)
    }

    @NotNull
    protected ZonedDateTime readDateRecorded() {
        byte[] dateFields = fetchHeaderField(0x0120, 10)

        Integer year = bytesToInt(dateFields[0..1] as byte[])
        Integer month = dateFields[2] as Integer
        Integer day = dateFields[3] as Integer
        Integer hour = dateFields[4] as Integer
        Integer minute = dateFields[5] as Integer
        Integer second = dateFields[6] as Integer
        Integer hundredths = dateFields[7] as Integer
        Integer micro = bytesToInt(dateFields[8..9] as byte[])
        // TODO check calc of nano is right
        //        Integer millis = ((hundredths * 10000 + micro) / 1000).intValue()
        Integer nano = (hundredths * 10000 + micro) * 1000
        LocalDateTime ldt = LocalDateTime.of(year, month, day, hour, minute, second, nano)
        ZoneId zoneId = calcTimeZone.call(ldt)

        return ZonedDateTime.of(ldt, zoneId)
    }

    @NotNull
    @AutoFinal
    protected Integer bytesToInt(@NotNull byte[] barray) {
        ByteBuffer buff = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN)
        buff.put(barray)
        int numZeros = 4 - barray.size()
        numZeros.times {
            buff.put(0 as byte)
        }
        buff.position(0)
        buff.int
    }

    @NotNull
    @AutoFinal
    protected Integer bytesToInt(byte b) {
        byte[] barray = new byte[1]
        barray[0] = b
        bytesToInt(barray)
    }

    @Nullable
    protected LocalDate readHeaderDate() {
        String datestr = fetchHeaderStringField(textHeaderOffset + 8, 8)
        if (!datestr) {
            return null
        }

        String year = datestr.substring(0, 4)
        String month = datestr.substring(4, 6)
        String day = datestr.substring(6)

        LocalDate.of(year.toInteger(), month.toInteger(), day.toInteger())
    }

    @AutoFinal
    protected void storeStringData(@Nullable String data, @NotNull Integer offset, @NotNull Integer length) {
        String padded = data?.padRight(length) ?: ' '.padRight(length)
        byte[] dataBytes = padded.bytes
        storeData(dataBytes, offset, length)
    }

    @AutoFinal
    protected void storeData(@Nullable byte[] data, @NotNull Integer offset, @NotNull Integer length) {
        Integer dataLength = data?.length ?: 0
        if (dataLength > length) {
            dataLength = length
        }
        if (dataLength > 0) {
            (0..<dataLength).each { Integer idx ->
                rawData[offset + idx] = data[idx]
            }
        }
        if (dataLength < length) {
            (dataLength..<length).each { Integer idx ->
                rawData[offset + idx] = 0x00
            }
        }
    }

    /**
     * Set the location name. Maximum 40 characters.
     * @param loc The location name.
     */
    @AutoFinal
    void setLocation(@Nullable String loc) {
        locn = checkStringLength(loc, locationMaxSize)
        storeStringData(locn, textHeaderOffset + 16, locationMaxSize)
    }

    @AutoFinal
    void setSpec(@Nullable String specStr) {
        specHd = checkStringLength(specStr, specMaxSize)
        storeStringData(specHd, textHeaderOffset + 106, specMaxSize)
    }

    /**
     * Set a value for the Tape field (device serial number).
     *
     * @param tapeStr The new value.
     */
    @AutoFinal
    void setTape(@Nullable String tapeStr) {
        tp = checkStringLength(tapeStr, tapeMaxSize)
        storeStringData(tp, textHeaderOffset, tapeMaxSize)
    }

    void setGpsDatum(@Nullable String datum) {
        gpsDtm = checkStringLength(datum, gpsDatumMaxSize)
        storeStringData(gpsDtm, 0x0130, gpsDatumMaxSize)
    }

    /**
     * Set the text version of the date in the header. This is different to the dateRecorded,
     * which is set by the Anabat.
     * @param newDate The date to be set
     */
    @AutoFinal
    void setHeaderDate(@Nullable LocalDate newDate) {
        headDate = newDate
        String dateStr = headDate?.format(headerDateFormat) ?: ''
        storeStringData(dateStr, textHeaderOffset + 8, 8)
    }

    /**
     * Set the note field. If more than 73 characters, will be stored in two fields. The first is 73 chars, the second 80.
     * The note will be split at the space char before the 73 chars. If there is no suitable space char,
     * will be split at 73 chars. If the amount remaining after the split is more than 80 chars, it will be
     * truncated.
     * @param newNote The new note.
     */
    void setNote(@Nullable String newNote) {
        String note1, note2
        if (newNote && newNote.length() > noteMaxSize) {
            def splitPlace = newNote.lastIndexOf(' ', noteMaxSize - 1)
            if (splitPlace < 1) {
                splitPlace = noteMaxSize - 1
            }
            note1 = checkStringLength(newNote.substring(0, splitPlace), noteMaxSize)
            note2 = checkStringLength(newNote.substring(splitPlace), note1MaxSize)
        } else {
            note1 = checkStringLength(newNote, noteMaxSize)
            note2 = checkStringLength(null, note1MaxSize)
        }
        storeStringData(note1, textHeaderOffset + 122, noteMaxSize)
        storeStringData(note2, textHeaderOffset + 122 + noteMaxSize, note1MaxSize)
    }

    /**
     * Save the metadata to a file.
     *
     * @param outfile the file to write to
     */
    @Override
    void save(File outfile) {
        byte[] newData = asByteArray()
        outfile.delete()
        outfile << newData
    }

    /**
     * Standard equals, based on datetime recorded and tape (serial number).
     *
     * @param o object to compare
     * @return whether the two objects have the same datetime recorded and device serial number
     */
    @Override
    boolean equals(o) {
        if (this.is(o)) {
            return true
        }
        if (!(o instanceof AnabatDataFileZC)) {
            return false
        }

        AnabatDataFileZC that = (AnabatDataFileZC) o

        if (getDateRecorded() != that.getDateRecorded()) {
            return false
        }
        if (getTape() != that.getTape()) {
            return false
        }

        return true
    }

    /**
     * Generate hashcode based on date recorded and device serial number.
     *
     * @return the hashcode
     */
    @Override
    int hashCode() {
        int result
        result = getDateRecorded().hashCode()
        result = 31 * result + getTape().hashCode()
        return result
    }

    /**
     * This file format is writable.
     *
     * @return true
     */
    @Override
    Boolean isWritable() {
        return true
    }
}
