/*
 * Copyright (c) 2014-2024. Yallara
 * This file is part of AnabatDataFile.
 *
 * AnabatDataFile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AnabatDataFile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AnabatDataFile.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.yallara.anabat

import groovy.transform.AutoFinal
import org.jetbrains.annotations.NotNull
import org.jetbrains.annotations.Nullable

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

/**
 * Represents a single .zc or .wav Anabat file. Allows read of both metadata formats, and write of .zc metadata.
 */
abstract class AnabatDataFileAbstract implements AnabatDataFile {
    protected static final int MAX_ALTITUDE = 9999
    protected static final int MIN_ALTITUDE = -999
    protected List<String> spcLst = []
    protected String locn
    protected String nte
    /**
     * Not the one in the data file header
     */
    protected ZonedDateTime dtRecorded
    /**
     *     the one in the header, entered manually
     */
    protected LocalDate headDate
    protected Integer divRatio
    protected String tp
    // first part of text header
    protected String specHd
    // from text header

    protected String gpsDtm
    protected BigDecimal lat
    protected BigDecimal lng
    protected Integer alti

    protected BigDecimal batVoltage
    protected BigDecimal temprtr

    protected final Closure<ZoneId> calcTimeZone

    protected final DateTimeFormatter headerDateFormat = DateTimeFormatter.ofPattern('yyyyMMdd')

    /**
     * Constructor.
     * @param calcTimeZone Optional Closure that returns a ZoneId for the timezone applicable to the data.
     *                     Passed one param: LocalDateTime
     *                     Default version always returns UTC.
     */
    @AutoFinal
    protected AnabatDataFileAbstract(Closure calcTimeZone = null) {
        this.calcTimeZone = calcTimeZone ?: { LocalDateTime ldt -> ZoneOffset.UTC } // Assume UTC
    }

    @NotNull
    List<String> getSpecies() {
        return spcLst
    }

    @Nullable
    ZonedDateTime getDateRecorded() {
        return dtRecorded
    }

    @Nullable
    Integer getDivRatio() {
        return divRatio
    }

    /**
     * Replace any existing species list with the passed one.
     *
     * @param speciesList The new list of species
     * @throws UnsupportedOperationException if file format is read-only (.wav)
     */
    @AutoFinal
    void setSpecies(@Nullable List<String> speciesList) throws UnsupportedOperationException {
        spcLst = speciesList ? new ArrayList<String>(speciesList) : [] as List<String>
        writeSpeciesList()
    }

    abstract protected void writeSpeciesList() throws UnsupportedOperationException

    /**
     * Add a single species to the existing list.
     *
     * @param newSpecies The new species to add.
     * @throws UnsupportedOperationException if file format is read-only (.wav)
     */
    @AutoFinal
    void addSpecies(@NotNull String newSpecies) throws UnsupportedOperationException {
        spcLst << newSpecies
        writeSpeciesList()
    }

    /**
     * Remove the specified species from the current species list. If the passed species
     * is not there, do nothing.
     *
     * @param oldSpecies Species to remove.
     * @throws UnsupportedOperationException if file format is read-only (.wav)
     */
    @AutoFinal
    void removeSpecies(@NotNull String oldSpecies) throws UnsupportedOperationException {
        spcLst -= oldSpecies
        writeSpeciesList()
    }

    /**
     * Latitude read from data file.
     *
     * @return Decimal latitude, negative for South
     */
    @Nullable
    BigDecimal getLatitude() {
        return lat
    }

    /**
     * Longitude read from data file.
     *
     * @return Decimal longitude, negative for West
     */
    @Nullable
    BigDecimal getLongitude() {
        return lng
    }

    /**
     * GPS Datum for lat/long. Not relevant for .wav.
     *
     * @return The datum name.
     */
    @Nullable
    String getGpsDatum() {
        return gpsDtm
    }

    /**
     * Returns altitude, in metres, read from metadata.
     *
     * @return altitude in metres.
     */
    Integer getAltitude() {
        return alti
    }

    @NotNull
    @AutoFinal
    protected String checkStringLength(@Nullable String source, @NotNull Integer maxSize) {
        String src = source?.trim() ?: ''
        src.size() > maxSize ? src.substring(0, maxSize) : src
    }

    /**
     * Return location string from metadata.
     *
     * Corresponds to 'Place Names' in .wav.
     *
     * @return the location, if set.
     */
    @AutoFinal
    @Nullable
    String getLocation() {
        return locn
    }

    @Nullable
    String getSpec() {
        return specHd
    }

    /**
     * Return the serial number of the device.
     *
     * @return device serial number
     */
    @Nullable
    String getTape() {
        return tp
    }

    /**
     * Get the temperature recorded by the device.
     *
     * @return the temperature, in Celsius.
     */
    @Nullable
    BigDecimal getTemperature() {
        return temprtr
    }

    /**
     * Get the battery voltage, in Volts.
     *
     * @return battery voltage
     */
    @Nullable
    BigDecimal getBatteryVoltage() {
        return batVoltage
    }

    /**
     * Return any notes within the metadata - .zc only.
     *
     * @return the notes.
     */
    @Nullable
    String getNote() {
        return nte
    }

    /**
     * Set the text version of the date in the header. This is different to the dateRecorded,
     * which is set by the Anabat. .zc files only.
     *
     * @param year The year
     * @param month The month
     * @param day The day
     * @throws UnsupportedOperationException if file format is read-only (.wav)
     */
    @AutoFinal
    void setHeaderDate(Integer year, Integer month, Integer day) throws UnsupportedOperationException {
        setHeaderDate(LocalDate.of(year, month, day))
    }

    /**
     * The date in the header of the file, which may not be the same as the date recoroded. - .zc files only.
     *
     * @return The date in the header, as a LocalDate.
     */
    @Nullable
    LocalDate getHeaderDate() {
        return headDate
    }
}
